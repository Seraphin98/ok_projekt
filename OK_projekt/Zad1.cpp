#include<iostream>
#include<fstream>
#include<ctime>
#include<cstdlib>
#include<vector>
#include<iomanip>
#include<math.h>
#include<string>


const int NR_TASKS = 50; 
const int FIND_BEST_LOOP = 50;
int MAIN_LOOP = 200; 
float PHER_RED_PARAMETER;

using namespace std;
 

class Machine {
public:
	int id[NR_TASKS];
	int task_duration[NR_TASKS];
	unsigned long int duration;
	void reset() {
		for (int a : id) {
			a = -3;
		}
		for (int a : task_duration) {
			a = -3;
		}
		duration = 0;
	}
};
class Task{
public:
	int id;
	int operation1;
	int operation2;
	void gen_operation() {
		operation1 = rand() % 15 + 5;
		operation2 = rand() % 15 + 5;
	}
};
class ToFile {
public:
	string duration;
	string to_file1;
	string to_file2;
	string maint1;
	string maint1_duration;
	string idle2;
	string idle2_duration;
	void reset() {
		to_file1 = "";
		to_file2 = "";
		maint1 = "";
		maint1_duration = "";
		idle2 = "";
		idle2_duration = "";
	}
	void copy(ToFile tofile) {
		duration = tofile.duration;
		to_file1 = tofile.to_file1;
		to_file2 = tofile.to_file2;
		maint1 = tofile.maint1;
		maint1_duration = tofile.maint1_duration;
		idle2 = tofile.idle2;
		idle2_duration = tofile.idle2_duration;
	}
};

int  min_time_id(Machine tab[FIND_BEST_LOOP]) {
	int min = 10000000;
	int id = 0;
	for (int i = 0; i < FIND_BEST_LOOP; i++) {
		if (tab[i].duration < min && tab[i].duration != 0) {
			id = i;
			min = tab[i].duration;
		}
	}
	return id;
}
void matrix_update(int *tab,int **matrix) {
	int from, to;
	for (int i = 0; i < NR_TASKS - 1; i++) {
		from = tab[i] -1;
		to = tab[i + 1] - 1;
		matrix[from][to] +=1;
	}
}
void matrix_reduction(int **matrix,float parameter) {
	for (int i = 0; i < NR_TASKS; i++) {
		for (int j = 0; j < NR_TASKS; j++) {
			matrix[i][j] = ceil(parameter * matrix[i][j]);
		}
	}
}
int matrix_max_id(int **matrix, int id) {
	int max = 0;
	int maxid = 0;
	for (int i = 0; i < NR_TASKS; i++) {
		if (matrix[id][i] > max) {
			max = matrix[id][i];
			maxid = i;
		}
	}
	return maxid;
}
bool ismaintenance() {
	int is_it = rand() % 2;
	if (is_it)return  true;
	else return false;
}
void matrix_display(int **matrix) {
	for (int i = 0; i<NR_TASKS; i++)cout << " " << i + 1 << " ";
	cout << endl;
	for (int i = 0; i<NR_TASKS; ++i) {
		cout << i + 1 << " ";
		for (int j = 0; j<NR_TASKS; ++j) {
			cout << " "<< matrix[i][j] << " ";
		}
		cout << endl;
	}
}
void tab_display(int *tab) {
	for (int i = 0; i <NR_TASKS; i++) {
		cout << tab[i] << "\t";
	}
}
bool id_exists(int id, vector<Task> tasks) {
	for(Task task : tasks) {
		if (task.id == id) return true;
	}
	return false;
}
int position_return(int id, vector<Task> tasks) {
	for (int i = 0; i < tasks.size(); i++) {
		if (tasks[i].id == id) return i;
	}
	return -1;
}
int rand_next_task(int* tab,vector<Task> tasks) {
	vector<int> tmptasks;
	tmptasks.erase(tmptasks.begin(), tmptasks.end());
	for (int i = 0; i < NR_TASKS; i++) {
		if (id_exists(i + 1, tasks)) {
			if (tab[i] == 0 || tab[i] == 1) {
				tmptasks.push_back(i + 1);
			}
			else {
				for (int j = 0; j < tab[i]; j++) {
					tmptasks.push_back(i + 1);
				}
			}
		}
	}
	int random = rand() % tmptasks.size();
	return tmptasks[random];
}
void write_tasks(vector<Task> tasks,int maintenance, string name, int nr) {
	name += ".txt";
	ofstream write(name);
	write << nr << endl;
	write << NR_TASKS << endl;
	for (Task task : tasks) {
		write << task.operation1 << ";" << task.operation2 << endl;
	}
	write << maintenance << endl;
	write.close();
}
void add_matrix(int** tab1, int** tab2) {
	for (int i = 0; i < NR_TASKS; i++) {
		for (int j = 0; j < NR_TASKS; j++) {
			tab1[i][j] += tab2[i][j];
			tab2[i][j] = 0;
		}
	}
}
void matrix_reset(int** matrix) {
	for (int i = 0; i < NR_TASKS; i++) {
		for (int j = 0; j < NR_TASKS; j++) {
			matrix[i][j] = 0;
		}
	}
}

string convert(int a) {
	return to_string(a);
}
void save_to_file(string name, ToFile tofile, int nr, int first_time) {
	name += ".txt";
	ofstream write(name);
	write << nr << endl;
	write << tofile.duration << ';' << first_time << endl;
	write << tofile.to_file1 << endl;
	write << tofile.to_file2 << endl;
	write << tofile.maint1 << ',' << tofile.maint1_duration << endl;
	write << convert(0) << ',' << convert(0) << endl;
	write << convert(0) << ',' << convert(0) << endl;
	write << tofile.idle2 << ',' << tofile.idle2_duration << endl;
	write.close();
	cout << "saved" << endl;
}
void main_program(int nr) {
	string wynik = "wynik";
	string inst = "instancja";
	int first_time;
	srand(time(NULL));
	//---------------------------------------------------------------- ZMIENNE
	Machine* machine1;
	Machine* machine2;
	machine1 = new Machine[FIND_BEST_LOOP];
	machine2 = new Machine[FIND_BEST_LOOP];
	vector<Task> task_tab;
	task_tab.reserve(NR_TASKS);
	vector<Task> tmp;
	int maintenance_counter = 0;
	int idle_counter = 0;
	ToFile tmptofile;
	ToFile finaltofile;
	string to_file1 = "";						//zmienne do zapisu do pliku wyj�ciowego 
	string to_file2 = "";						//zmienne do zapisu do pliku wyj�ciowego 
	int min;
	int idle_duration = 0;
	int maint_duration = 0;	
	int task_id = 0;							//zmienna do przechowywania id zadania w maszynie
	int avrgtime1 = 0;							//zmienne do liczenia �redniego czasu
	int wait_duration;							//zmienna do przechowywania czasu oczekiwania na mszynie 2
	int maintanace1;							//zmienna do przechowywania czasu maintenece'u dla 1 maszyny
	int task_nr;								//zmienna do przechowywania numeru zadania podczas trwania algorytmu
	int next_task;								//zmienna do przechowywania numeru nastepnego zadania wyczytanego z macierzy feromonow
	double cumulation = 0;						//zmienna do przechowywania wartosci kumulacji
	bool randflag = false;						//zmienna do kontrolowania losowania lub wyboru nowego zadania
	bool cumulationflag = true;					//zmienna do kontrolowania kumulacji
	int **ant_matrix = new int*[NR_TASKS];		//tworzenie i zerowanie macierzy feromonow
	int **tmpant_matrix = new int*[NR_TASKS];
	for (int i = 0; i < NR_TASKS; i++) {
		ant_matrix[i] = new int[NR_TASKS];
		tmpant_matrix[i] = new int[NR_TASKS];
		for (int j = 0; j < NR_TASKS; j++) {
			ant_matrix[i][j] = 0;
			tmpant_matrix[i][j] = 0;
		}
	}
	//----------------------------------------------------------------PROGRAM
				//zapis wygenerowanych zada� do pliku
	min = INT_MAX;
	PHER_RED_PARAMETER = 0.1;
	for (int loop = 0; loop < 11; loop++) {
		PHER_RED_PARAMETER = PHER_RED_PARAMETER + 0.1;
		inst = "instancja" + convert(loop);
		task_tab.clear();
		avrgtime1 = 0;
		Task task;
		for (int i = 0; i < NR_TASKS; i++) {		//generowanie zadan
			task.id = i + 1;
			task.gen_operation();
			avrgtime1 += task.operation1;
			task_tab.push_back(task);
		}
		maintanace1 = (avrgtime1 / NR_TASKS) * 3;					//obliczanie czasu maintanence'a
		write_tasks(task_tab, maintanace1, inst, nr);
		for (int k = 0; k < 11; k++) {
			wynik = "wynik" + convert(loop) + '_' +  convert(k);
			for (int i = 0; i < MAIN_LOOP; i++) {
				for (int j = 0; j < FIND_BEST_LOOP; j++) {
					maintenance_counter = 0;
					idle_counter = 0;
					tmptofile.to_file1 = "M1:";
					tmptofile.to_file2 = "M2:";
					idle_duration = 0;
					maint_duration = 0;
					task_id = 0;
					machine1[j].reset();
					machine2[j].reset();
					tmp.clear();
					tmp.assign(task_tab.begin(), task_tab.end());
					cumulation = 0;
					cumulationflag = true;
					while (!tmp.empty()) {
						if (tmp.size() == NR_TASKS) {
							task_nr = position_return(rand_next_task(ant_matrix[rand() % NR_TASKS], tmp), tmp);
						}
						else {
							task_nr = next_task;
						}
						machine1[j].id[task_id] = tmp[task_nr].id;
						tmptofile.to_file1 += "op1_" + convert(tmp[task_nr].id) + ',' + convert(machine1[j].duration) + ',';
						machine1[j].duration += tmp[task_nr].operation1;						//doliczanie czasu na maszynie1
						tmptofile.to_file1 += convert(tmp[task_nr].operation1) + ',';
						if (!cumulationflag) {
							machine1[j].duration += ceil(tmp[task_nr].operation1*cumulation);	//dodawanie czasu z kumulacji je�eli takowa istnieje
							machine1[j].task_duration[task_id] = ceil(tmp[task_nr].operation1*cumulation) + tmp[task_nr].operation1;
							tmptofile.to_file1 += convert(machine1[j].task_duration[task_id]) + ';';
						}
						else
						{
							tmptofile.to_file1 += convert(tmp[task_nr].operation1) + ';';
						}
						if (ismaintenance()) {
							machine1[j].duration += maintanace1;
							cumulationflag = true;
							cumulation = 0;
							tmptofile.to_file1 += "maint" + convert(maintenance_counter + 1) + '_' + 'M' + convert(1) + ',' + convert(machine1[j].duration) + ',' + convert(maintanace1) + ';';
							maintenance_counter++;
							maint_duration += maintanace1;
						}
						else {
							cumulationflag = false;
							cumulation += 0.1;
						}
						if (machine1[j].duration > machine2[j].duration) {
							wait_duration = machine1[j].duration - machine2[j].duration;
							idle_duration += wait_duration;
							tmptofile.to_file2 += "idle" + convert(idle_counter + 1) + '_' + 'M' + convert(2) + ',' + convert(machine2[j].duration) + ',' + convert(wait_duration) + ';';
							idle_counter++;
						}
						else {
							wait_duration = 0;
						}
						machine2[j].id[task_id] = tmp[task_nr].id;
						machine2[j].duration += wait_duration;
						tmptofile.to_file2 += "op2_" + convert(machine2[j].id[task_id]) + ',' + convert(machine2[j].duration) + ',' + convert(tmp[task_nr].operation2) + ',' + convert(tmp[task_nr].operation2) + ';';
						machine2[j].duration += tmp[task_nr].operation2;
						tmp.erase(tmp.begin() + task_nr);
						task_id++;
						if (tmp.size())next_task = position_return(rand_next_task(ant_matrix[task_nr], tmp), tmp);
					}
					if (i == 0 && j == 0) first_time = machine2[j].duration;
					tmptofile.idle2 = convert(idle_counter);
					tmptofile.idle2_duration = convert(idle_duration);
					tmptofile.maint1 = convert(maintenance_counter);
					tmptofile.maint1_duration = convert(maint_duration);
					tmptofile.duration = convert(machine2[j].duration);
					if (min > machine2[j].duration) {
						min = machine2[j].duration;
						finaltofile.copy(tmptofile);
					}
				}
				matrix_update(machine2[min_time_id(machine2)].id, tmpant_matrix);
				cout << machine2[min_time_id(machine2)].duration << endl;
				if (i % 5 == 0 && i != 0) add_matrix(ant_matrix, tmpant_matrix);
				matrix_reset(tmpant_matrix);
				if (i % 10 == 0 && i != 0) matrix_reduction(ant_matrix, PHER_RED_PARAMETER);
			}
			save_to_file(wynik, finaltofile, nr, first_time);
			matrix_reset(ant_matrix);
			min = INT_MAX;
			//wynik = "wynik";
		}
	}
}
int main() {
	main_program(1);
	return 0;
}
